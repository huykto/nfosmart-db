/* Owner: NfoSmart
 * Creator: Huy To
*/
/* drop TABLEs */
DROP TABLE IF EXISTS recipient;
DROP TABLE IF EXISTS card_content;
DROP TABLE IF EXISTS unique_card;
DROP TABLE IF EXISTS account_notification;
DROP TABLE IF EXISTS contact;
DROP TABLE IF EXISTS security_answer;
DROP TABLE IF EXISTS security_question;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS account_type;
DROP TABLE IF EXISTS card_content_type;
DROP TABLE IF EXISTS card_design;
DROP TABLE IF EXISTS optical_code;
DROP TABLE IF EXISTS code_category;
DROP TABLE IF EXISTS occassion;
