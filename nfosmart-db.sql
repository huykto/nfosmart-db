/* Owner: NfoSmart
 * Creator: Huy To
*/

/* account_type TABLE */
CREATE TABLE IF NOT EXISTS account_type(
   account_type_id int PRIMARY KEY, -- record id
   account_type varchar( 20 ) NOT NULL,
   create_on timestamptz NOT NULL DEFAULT NOW(),
   updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* account TABLE */
CREATE TABLE IF NOT EXISTS account(
  account_id int PRIMARY KEY, -- record id
  email varchar ( 255) UNIQUE NOT NULL , -- PRIMARY KEY
  password varchar ( 50 ) NOT NULL,
  account_type_id int NOT NULL REFERENCES account_type(account_type_id), -- foreign key to account_type table
  name varchar ( 255 )  NOT NULL, -- full name ?
  phone varchar ( 15 ) NOT NULL,
  send_phone_notifications boolean, -- Y or N
  send_email_notifications boolean, -- Y or N
  terms_accept text NOT NULL,
  last_login timestamptz NOT NULL DEFAULT NOW(),
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);

/* account_notification TABLE */
CREATE TABLE IF NOT EXISTS account_notification(
 notification_id int PRIMARY KEY, -- record id
 account_id int REFERENCES account(account_id),
 message text,
 create_on timestamptz NOT NULL DEFAULT NOW(),
 update_on timestamptz NOT NULL DEFAULT NOW(),
 read_on timestamptz
);
/* occassion TABLE */
CREATE TABLE IF NOT EXISTS occassion(
  occassion_id int PRIMARY KEY, -- record id
  occassion_name varchar( 100 ),
  description text,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* card_design TABLE */
CREATE TABLE IF NOT EXISTS card_design(
  card_design_id int PRIMARY KEY, -- record id
  occassion_id int REFERENCES occassion(occassion_id),
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* unique_card TABLE */
CREATE TABLE IF NOT EXISTS unique_card(
  card_id int PRIMARY KEY, -- record id
  account_id int REFERENCES account(account_id),
  card_design_id int REFERENCES card_design(card_design_id),
  unique_id int UNIQUE ,
  custom_cover_text text,
  custom_inside_text text,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* card_content_type TABLE */
CREATE TABLE IF NOT EXISTS card_content_type(
  card_content_type_id int PRIMARY KEY, -- record id
  description text,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* card_content TABLE */
CREATE TABLE IF NOT EXISTS card_content(
  card_content_id int PRIMARY KEY, -- record id
  card_id int REFERENCES unique_card(card_id),
  card_content_type_id int REFERENCES card_content_type(card_content_type_id),
  card_content varchar ( 255 ) NOT NULL, -- card binary content
  private boolean,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* code_category TABLE */
CREATE TABLE IF NOT EXISTS code_category(
  code_category_id int PRIMARY KEY, -- record id
  description text,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* contact TABLE */
CREATE TABLE IF NOT EXISTS contact(
 contact_id int PRIMARY KEY, -- record id
 account_id int REFERENCES account(account_id),
 contact_name varchar ( 255 ) NOT NULL,
 birthday varchar ( 5 ) NOT NULL, --day and month no year: 12-05
 street1 varchar ( 100 ) NOT NULL,
 street2 varchar ( 100 ),
 city varchar ( 60 ) NOT NULL,
 state varchar ( 50 ) NOT NULL,
 zip varchar ( 9 ) NOT NULL,
 create_on timestamptz NOT NULL DEFAULT NOW(),
 updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* optical_code TABLE */
CREATE TABLE IF NOT EXISTS optical_code(
  optical_code_id int PRIMARY KEY, -- record id
  code_category_id int REFERENCES code_category(code_category_id),
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* recipient TABLE */
CREATE TABLE IF NOT EXISTS recipient(
  recipient_id int PRIMARY KEY, -- record id
  contact_id int REFERENCES contact(contact_id),
  card_id int REFERENCES unique_card(card_id),
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* security_question TABLE */
CREATE TABLE IF NOT EXISTS security_question(
  security_question_id int PRIMARY KEY, -- record id
  question_text varchar ( 50 ) NOT NULL,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
/* security_answer TABLE */
CREATE TABLE IF NOT EXISTS security_answer(
  security_answer_id int PRIMARY KEY, -- record id
  security_question_id int REFERENCES security_question(security_question_id),
  answer_text varchar ( 50 ) NOT NULL,
  account_id int REFERENCES account(account_id),
  sequence int NOT NULL,
  create_on timestamptz NOT NULL DEFAULT NOW(),
  updated_on timestamptz NOT NULL DEFAULT NOW()
);
